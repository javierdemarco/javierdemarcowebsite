# Javier de Marco Website
![WebsiteHome](images/websitehome "Website Home")

-------------------------------------------------------------------------------

This is my personal website.
The language used to program the website is [Hugo](https://gohugo.io/ "Hugo Oficial Language Website"), a fast and flexible static site generator written in go.

-------------------------------------------------------------------------------

## Contents
1. Projects
2. Personal Wiki
3. About
4. Contact

-------------------------------------------------------------------------------

### Projects
This is where i post my personal projects. They should all be hosted on gitlab and github where you can see the source code.

### Personal Wiki
A wiki i try to maintain with computer, programming and Linux related knowledge i want to have always available so i can read it at any time.

### About
Mostly my _Curriculum Vitae_

### Contact
All the links and email accounts where you can contact me.
